def nums_2tiles(numlist):
    tiles=[]
    for a in numlist:
        if a=='_':
            tiles.append('t_empty')
        else:
            tiles.append('t'+a)
    return tiles

def read_inputs_func(finp):
    inputs={}
    with open(finp, 'r') as fp:
        content = fp.read().splitlines()
        inputs['alg']=content[0]
        if content[2]=='with open':
            inputs['print_open']=True
        else:
            inputs['print_open']=False
        inputs['puzzle_size']=(int(content[3][0]),int(content[3][2]))
        assert('Black' in content[4])
        a=content[4].split(': ')
        if len(a)>1:
            b=a[1].split(',')
        else:
            b=[]
        inputs['Black']=nums_2tiles(b)
        assert('Red' in content[5])
        a = content[5].split(': ')
        if len(a) > 1:
            b = a[1].split(',')
        else:
            b = []
        inputs['Red']=nums_2tiles(b)
        inputs['start_state']=[]
        for i in range(inputs['puzzle_size'][0]):
            inputs['start_state'].append(nums_2tiles(content[6+i].split(',')))
        return inputs

green_cost=1
red_cost=30
inputs_dict=read_inputs_func('input.txt')
alg=inputs_dict['alg']
print_open=inputs_dict['print_open']
num_rows=inputs_dict['puzzle_size'][0]
num_cols=inputs_dict['puzzle_size'][1]
Black=inputs_dict['Black']
Red=inputs_dict['Red']
for tile in Black:
    assert(not (tile in Red) )
start_state=inputs_dict['start_state']
a=7