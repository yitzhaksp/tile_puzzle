from read_inputs import *

operators=['L','U','R','D']
opposite_op={'R':'L','L':'R','U':'D','D':'U'}


def copy_state(state):
    state_copy = []
    for row in state:
        state_copy.append(row.copy())
    return state_copy

def state_to_id(state):
    id=''
    for row in state:
        for tile in row:
            id+=tile
    return id


def is_goalstate(state):
    goaltile_num=1
    num_tiles=num_rows*num_cols
    for row in state:
        for tile in row:
            if goaltile_num< num_tiles: #ignore the last tile (i=num_tiles)
                goaltile='t'+str(goaltile_num)
                if tile!=goaltile:
                    return False
            goaltile_num+=1
    return True

def get_empty_pos(state):
    for i,row in enumerate(state):
        for j,tile in enumerate(row):
            if tile=='t_empty':
                return (i,j)
    raise Exception("error: no empty tile in puzzle")

def get_tiletomove_pos(op,state):
    assert (op in operators)
    empty_pos = get_empty_pos(state)
    if op=='L':
        ttmpos=(empty_pos[0],empty_pos[1]+1)
    elif op=='R':
        ttmpos=(empty_pos[0],empty_pos[1]-1)
    elif op=='U':
        ttmpos=(empty_pos[0]+1,empty_pos[1])
    elif op=='D':
        ttmpos=(empty_pos[0]-1,empty_pos[1])
    if ttmpos[0]<0 or ttmpos[0]>(num_rows-1):
        return 'invalid_pos'
    if ttmpos[1]<0 or ttmpos[1]>(num_cols-1):
        return 'invalid_pos'
    return ttmpos


def is_validop(op,state):
    assert(op in operators)
    ttm_pos=get_tiletomove_pos(op,state)
    if ttm_pos=='invalid_pos':
        return False
    elif state[ttm_pos[0]][ttm_pos[1]] in Black:
        return False
    return True


def apply_op_tostate(op,state):
    assert(op in operators)
    new_state = copy_state(state)
    ttm_pos = get_tiletomove_pos(op, state)
    assert(ttm_pos!='invalid_pos')
    empty_pos = get_empty_pos(state)
    new_state[empty_pos[0]][empty_pos[1]]=state[ttm_pos[0]][ttm_pos[1]]
    new_state[ttm_pos[0]][ttm_pos[1]]='t_empty'
    return new_state

def compute_opcost(op,state):
    ttm_pos = get_tiletomove_pos(op, state)
    if state[ttm_pos[0]][ttm_pos[1]] in Red:
        return red_cost
    return green_cost


def get_valid_operators(state):
    valid_ops=[]
    for op in operators:
        if is_validop(op,state):
            valid_ops.append(op)
    return valid_ops

class node(object):
    def __init__(self,state,path,cost):
        self.state=copy_state(state)
        self.id=state_to_id(state)
        self.path=path.copy()
        self.cost=cost
        self.possible_ops=self.get_possible_operators()
        self.num_rows=len(state)
        self.num_cols=len(state[0])

    def create_copy(self):
        ncopy=node(self.state,self.path,self.cost)
        return ncopy

    def get_possible_operators(self):
        valid_ops=get_valid_operators(self.state)
        if self.path: # path not empty?
            possible_ops=[]
            for op in valid_ops:
                if not (op==opposite_op[self.path[-1][1]]):
                    possible_ops.append(op)
        else:
            possible_ops=valid_ops
        return possible_ops

    def generate_child(self,op):
        assert(op in self.possible_ops)
        new_state=apply_op_tostate(op,self.state)
        ttm_pos=get_tiletomove_pos(op,self.state)
        ttm=self.state[ttm_pos[0]][ttm_pos[1]]
        op_cost=compute_opcost(op,self.state)
        child=node(new_state,self.path+[(ttm,op)],self.cost+op_cost)
        return child

    def is_goal(self):
        return is_goalstate(self.state)