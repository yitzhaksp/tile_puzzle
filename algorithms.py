from queue import Queue,PriorityQueue
from collections import deque
from graph_utils import *
from other_utils import *
inf_repl=1230000
max_stacksize=10000

def BFS(nstart,print_open):
    # Breadth first search
    num_nodes=0
    it=0
    expanded=[]
    L=Queue()
    L.put(nstart)
    while L.qsize()>0:
        it+=1
        if L.qsize()> max_stacksize:
            raise Exception('memory exhausted')
        if (divmod(it,1000)[1]==0):
            print('iteration: {}'.format(it))
        if print_open:
            print_thelist(L, it)
        n=L.get()
        if n.is_goal():
            return True, n.path, n.cost, num_nodes
        #print('expanding node '+n.id)
        if not (n.id in expanded):
            for op in n.possible_ops:
                child=n.generate_child(op)
                num_nodes+=1
                L.put(child)
            expanded.append(n.id)
    return False,None,None, num_nodes



def DLS(n,visited, depth_limit,num_nodes):
    # depth limited search
    # 'visited' will not contain all visited states
    # but only those, required for loop avoidance
    new_num_nodes=num_nodes
    #cost='dummy'
    if n.is_goal():
        return True,n.path,n.cost,new_num_nodes
    elif depth_limit==0:
        return False,None,None,new_num_nodes
    visited.append(n.id)
    for op in n.possible_ops:
        child = n.generate_child(op)
        new_num_nodes+=1
        if child.id in visited:
            continue
        found,path,cost,new_num_nodes=DLS(child,visited,depth_limit-1,new_num_nodes)
        if found:
            return True,path,cost,new_num_nodes
    visited.remove(n.id)
    return False,None,None,new_num_nodes


# uses DLS()
def DFID(nstart,max_depth):
    # often also called IDS
    num_nodes_total=0
    for depth_limit in range(max_depth+1):
        visited=[]
        print('running DLS with depth_limit {}'.format(depth_limit))
        found,path,cost,num_nodes=DLS(nstart,visited,depth_limit,0)
        num_nodes_total+=num_nodes
        if found:
            return True,path,cost,num_nodes_total
    return False,None,None,num_nodes_total


def Astar(nstart,f,print_open):
    expanded=[]
    it=0
    num_nodes=0
    open_list=PriorityQueue()
    # the only reason why we put num_nodes in open_list
    # is in order for priority queue to function in case that f(n1)==f(n2)
    # in that case priority queue will compare num_nodes1 to num_nodes2
    # the actual num_node is irelevant. the only important thing is that
    # these numbers are unique for each element in the priority_queue.
    open_list.put( (f(nstart),num_nodes,nstart)  )
    while open_list.qsize()>0:
        it+=1
        print('iteration: {}'.format(it))
        if print_open:
            print_thelist(open_list,it)
        n=open_list.get()[2]
        if n.is_goal():
            return True, n.path, n.cost, num_nodes
        if not (n.id in expanded):
            for op in n.possible_ops:
                child=n.generate_child(op)
                num_nodes+=1
                #tmp1=(f(child),child)
                open_list.put( (f(child),num_nodes,child)  )
            expanded.append(n.id)
    return False, None, None, num_nodes


def IDAstar(nstart,f,print_open):
    num_nodes=0
    it=0
    L=deque()
    H={}
    t=f(nstart)
    nstart.out=False
    if nstart.is_goal():
        return True, nstart.path, nstart.cost, num_nodes
    while t!=inf_repl:
        it+=1
        print('iteration: {}'.format(it))
        print('t={}'.format(t))
        if print_open:
            print_thelist(L, it)
        fmin=inf_repl
        L.append(nstart)
        H[nstart.id]=nstart
        while L: #not empty
            n_orig=L.pop()
            n=n_orig.create_copy()
            n.out=n_orig.out
            if n.out:
                H.pop(n.id,None) #remove n.id key
            else:
                n.out=True
                L.append(n)
                for op in n.possible_ops:
                    child = n.generate_child(op)
                    child.out=False
                    #print('fchild={}'.format(f(child)))
                    num_nodes += 1
                    if f(child)>t:
                        fmin=min(fmin,f(child))
                        continue
                    if child.id in H:
                        gprime=H[child.id]
                        if gprime.out:
                            continue
                        else:
                            if f(gprime)>f(child):
                                H.pop(gprime.id,None)
                                L.remove(gprime)
                            else:
                                continue
                    if child.is_goal():
                        return True, child.path, child.cost, num_nodes
                    L.append(child)
                    H[child.id]=child
        t=fmin
    return False, None, None, num_nodes


def DFBnB(nstart,f,print_open):
    num_nodes = 0
    it = 0
    L = deque()
    H = {}
    t = f(nstart)
    nstart.out = False
    if nstart.is_goal():
        return True, nstart.path, nstart.cost, num_nodes
    L.append(nstart)
    H[nstart.id] = nstart
    t=inf_repl
    result=(False,None,None,None)
    while L:  # not empty
        it+=1
        if (divmod(it,1000)[1]==0):
            print('iteration: {}'.format(it))
        if len(L)> max_stacksize:
            raise Exception('open list has become too large. memory exhausted.')
        if print_open:
            print_thelist(L, it)
        n_orig = L.pop()
        n = n_orig.create_copy()
        n.out = n_orig.out
        if n.out:
            H.pop(n.id, None)  # remove n.id key
        else:
            n.out = True
            L.append(n)
            children = []
            for op in n.possible_ops:
                child=n.generate_child(op)
                child.out=False
                children.append(child)
            num_nodes+=len(n.possible_ops)
            children_srt=sorted(children,key=f)
            children=None # for safety
            for i,child in enumerate(children_srt):
                if f(child)>=t:
                    children_srt=children_srt[:i]
                    break
                elif child.id in H:
                    gprime = H[child.id]
                    if gprime.out:
                        children_srt.remove(child)
                    else:
                        if f(gprime)<=f(child):
                            children_srt.remove(child)
                        else:
                            H.pop(gprime.id, None)
                            L.remove(gprime)
                elif child.is_goal():
                    t=f(child)
                    print('solution discovered !')
                    print('searching for cheaper solution ...')
                    result=(True, child.path, child.cost, num_nodes)
                    children_srt=children_srt[:i]
            for nd in reversed(children_srt):
                L.append(nd)
                H[nd.id]=nd
    return result




