from read_inputs import *


def print_thelist(L,it):
    print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    print('iteration: {}'.format(it))
    print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    print('open list:')
    try:
        for b in L.queue:
            print(b.state)
    except AttributeError:
        try:
            for b in L:
                print(b.state)
        except TypeError as err:
            print('unable to print open list.')
            print(err)

def comp_correct_pos(tile_num,num_rows,num_cols):
    div,mod=divmod(tile_num-1,num_cols)
    row=div
    col=mod
    return (row,col)

def h(n):
    dist_sum=0
    for rowtile,row in enumerate(n.state):
        for coltile,tile in enumerate(row):
            if tile=='t_empty':
                continue
            else:
                (rownum_corr,colnum_corr)=comp_correct_pos(int(tile[1:]),num_rows,num_cols)
                dist_sum+=abs(rowtile-rownum_corr)+abs(coltile-colnum_corr)
    return dist_sum

def f(n):
    return n.cost+h(n)

def fgreedy(n):
    return n.cost

def path_2string(path):
    s=""
    for i,mv in enumerate(path):
        s+=mv[0][1:]+mv[1]
        s+='-'
        if divmod(i+1,5)[1]==0:
            s+='\n'
    s += '\n'
    return s