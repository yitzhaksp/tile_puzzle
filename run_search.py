from graph_utils import *
from algorithms import *
from read_inputs import *
from other_utils import *
import time

for row in start_state:
    assert(len(row)==num_cols)
possible_algs=['BFS','DFID','Astar','IDAstar','DFBnB']
if not (alg in possible_algs):
    print('invalid algorithm.')
    print('algorithm must be one of the following:')
    print(possible_algs)
    exit()
nstart=node(start_state,[],0)
start_time = time.time()
if alg=='BFS':
    found,solution,cost,num_nodes=BFS(nstart,print_open)
if alg=='DFID':
    max_depth=50
    found,solution,cost,num_nodes=DFID(nstart,max_depth)
if alg=='Astar':
    found,solution,cost,num_nodes=Astar(nstart,f,print_open)
if alg=='IDAstar':
    found,solution,cost,num_nodes=IDAstar(nstart,f,print_open)
if alg=='DFBnB':
    found,solution,cost,num_nodes=DFBnB(nstart,f,print_open)
elapsed_time = time.time() - start_time
if found:
    print('solution: {}'.format(solution))
    print('cost: {}'.format(cost))
    with open('output.txt', 'w') as fp:
        s=path_2string(solution)
        fp.write(s)
        fp.write('Num: {} \n'.format(num_nodes))
        fp.write('Cost: {} \n'.format(cost))
        fp.write('Time: {} s'.format(round(elapsed_time,5)))
else:
    print('no solution found')
    with open('output.txt', 'w') as fp:
        fp.write("no path \n")
        fp.write('Num: {} \n'.format(num_nodes))
        fp.write('Time: {} s'.format(round(elapsed_time,5)))
print('created output.txt')
a=7
